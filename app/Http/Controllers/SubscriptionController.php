<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Clients;
use App\Models\Delete;
use Illuminate\Support\Facades\DB;
use MongoDB\BSON\Type;

class SubscriptionController extends Controller
{
    public function index1(Request $request)
    {
        $type = $request->input('type', null);
        $regular = $request->input('regular', null);
        $model_clients = new Clients();
        $clients = $model_clients->getClients($type, $regular);
        return view('client.list', [
            'clients' => $clients,
            'abonement_types' => Clients::$abonement_types,
            'regular_types' => Clients::$regular_types,
            'type_selected' => $type,
            'regular_selected' => $regular

        ]);
    }

    public function client($id)
    {
        $model_clients = new Clients();
        $client = $model_clients->getClientByID($id);
        return view('client.client')->with('client', $client);
    }

    public function contacts()
    {
        return view('layouts.contacts');
    }


    public function adding(Request $request = NULL)
    {
//        $reg = 1;
        $fio = $request->input('name', null);
        $subs = $request->input('subs_id', null);
        $telephone = $request->input('tel', null);

        DB::update('insert into client set name = ?,tel=?, type_sbs=?',[$fio,$telephone,$subs]);
        echo "Record updating successfully. Please wait DB initialize...";
        return view('layouts.loading');

    }

    public function destroy(Request $request) {
        $id = $request->input('client_id', null);
        DB::delete('delete from client where client_id = ?',[$id]);
        echo "Record deleted successfully. Please wait DB initialize...";

        return view('layouts.loading');
    }

    public function editing(Request $request){
        return view('client.edit');
        $id = $request->input('client_id', null);
        $name = $request->input('name', null);
        $tel = $request->input('tel', null);
        $type_sbss = $request->input('type_sbss', '4');
        $datee = $request->input('datee', null);
        $dates = $request->input('dates', null);

        DB::delete('UPDATE client SET client_id = ?, name = ?, tel = ?, type_sbs = ?, date_s = ?, date_e = ? where client_id = ?',[$id,$name,$tel,$type_sbss,$datee,$dates]);
        echo "Record deleted successfully. Please wait DB initialize...";
        return view('layouts.loading');
    }

    public function editingconfirm(Request $request){

        $id = $request->input('client_id', null);
        $name = $request->input('name', null);
        $tel = $request->input('tel', null);
        $type_sbss = $request->input('type_sbss', '4');
        $datee = $request->input('datee', null);
        $dates = $request->input('dates', null);

        DB::delete('UPDATE client SET client_id = ?, name = ?, tel = ?, type_sbs = ?, date_s = ?, date_e = ? where client_id = ?',[$id,$name,$tel,$type_sbss,$datee,$dates,$id]);
        echo "Record updating successfully. Please wait DB initialize...";
        return view('layouts.loading');
    }


}
