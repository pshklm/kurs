
<style type="text/css">
    h1 {
        font-size: 120%;
        font-family: Verdana, Arial, Helvetica, sans-serif;
        color: #333366;
    }
    .container:after {
        content: "";
        display: table;
        clear: both;
    }
    .logo {
        float: left;
    }
    nav {
        float: right;
    }
    nav ul {
        margin: 0;
        padding: 0;
        list-style: none;
    }
    nav li {
        display: inline-block; /*один из способов разместить элементы в строку*/
    }
    .container {
        width: 100%;
        max-width: 1024px;
        padding: 30px;
        margin: 0 auto;
    }
    nav a {
        text-decoration: none;
        line-height: 38px;
    }

    @font-face {
        font-family: 'handwriting';
        src: url('fonts/journal-webfont.woff2') format('woff2'),
        url('fonts/journal-webfont.woff') format('woff');
        font-weight: normal;
        font-style: normal;
    }

    @font-face {
        font-family: 'typewriter';
        src: url('fonts/veteran_typewriter-webfont.woff2') format('woff2'),
        url('fonts/veteran_typewriter-webfont.woff') format('woff');
        font-weight: normal;
        font-style: normal;
    }

    body {
        font  : 21px sans-serif;

        padding : 2em;
        margin  : 2;

        background: #99ccfc;
    }

    table {
        font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
        text-align: left;
        border-collapse: separate;
        border-spacing: 5px;
        background: #ECE9E0;
        color: #656665;
        border: 16px solid #ECE9E0;
        border-radius: 20px;
    }
    .select-css {
        display: block;
        font-size: 16px;
        font-family: sans-serif;
        font-weight: 700;
        color: #444;
        line-height: 1.3;
        padding: .6em 1.4em .5em .8em; width: 100%;
        max-width: 100%;
        box-sizing: border-box;
        margin: 0;
        border: 1px solid #aaa;
        box-shadow: 0 1px 0 1px rgba(0,0,0,.04);
        border-radius: .5em;
        -moz-appearance: none;
        -webkit-appearance: none;
        appearance: none;
        background-color: #fff;
        background-image: url('data:image/svg+xml;charset=US-ASCII,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22292.4%22%20height%3D%22292.4%22%3E%3Cpath%20fill%3D%22%23007CB2%22%20d%3D%22M287%2069.4a17.6%2017.6%200%200%200-13-5.4H18.4c-5%200-9.3%201.8-12.9%205.4A17.6%2017.6%200%200%200%200%2082.2c0%205%201.8%209.3%205.4%2012.9l128%20127.9c3.6%203.6%207.8%205.4%2012.8%205.4s9.2-1.8%2012.8-5.4L287%2095c3.5-3.5%205.4-7.8%205.4-12.8%200-5-1.9-9.2-5.5-12.8z%22%2F%3E%3C%2Fsvg%3E'), linear-gradient(to bottom, #ffffff 0%,#e5e5e5 100%);
        background-repeat: no-repeat, repeat;
        background-position: right .7em top 50%, 0 0;
        background-size: .65em auto, 100%;
    }
    .select-css::-ms-expand { display: none; }
    .select-css:hover { border-color: #888; }
    .select-css:focus { border-color: #aaa;
        box-shadow: 0 0 1px 3px rgba(59, 153, 252, .7);
        box-shadow: 0 0 0 3px -moz-mac-focusring;
        color: #222;
        outline: none;
    }
    .select-css option { font-weight:normal; }
    *[dir="rtl"] .select-css, :root:lang(ar) .select-css, :root:lang(iw) .select-css {
        background-position: left .7em top 50%, 0 0;
        padding: .6em .8em .5em 1.4em;
    }
    th {
        font-size: 18px;
        padding: 10px;
    }
    td {
        background: #F5D7BF;
        padding: 10px;
    }

    form {
        position: relative;

        width  : 1000px;
        height : 100px;
        margin : 0 auto;

        /*background: #FFF url(background.jpg);*/
    }
    /*input[type=text] {*/
    /*    padding:10px;*/
    /*    border:0;*/
    /*    box-shadow:0 0 15px 4px rgba(0,0,0,0.06);*/
    /*}*/

    input[type=submit] {
        text-decoration: none;
        display: inline-block;
        color: white;
        padding: 20px 30px;
        margin: 10px 20px;
        border-radius: 10px;
        font-family: 'Montserrat', sans-serif;
        text-transform: uppercase;
        letter-spacing: 2px;
        background-image: linear-gradient(to right, #9EEFE1 0%, #4830F0 51%, #9EEFE1 100%);
        background-size: 200% auto;
        box-shadow: 0 0 20px rgba(0, 0, 0, .1);
        transition: .5s;
    }
    input[type=submit]:hover {
        background-position: right center;
    }
    /*a {*/
    /*    text-decoration: none;*/
    /*    display: inline-block;*/
    /*    color: white;*/
    /*    padding: 20px 30px;*/
    /*    margin: 10px 20px;*/
    /*    border-radius: 10px;*/
    /*    font-family: 'Montserrat', sans-serif;*/
    /*    text-transform: uppercase;*/
    /*    letter-spacing: 2px;*/
    /*    background-image: linear-gradient(to right, #9EEFE1 0%, #4830F0 51%, #9EEFE1 100%);*/
    /*    background-size: 200% auto;*/
    /*    box-shadow: 0 0 20px rgba(0, 0, 0, .1);*/
    /*    transition: .5s;*/
    /*}*/
    /*a:hover {*/
    /*    background-position: right center;*/
    /*}*/

    input {
        padding:10px;
        border-radius:10px;
    }

    .cssload-cssload-wrap2 {
        width: 281px;
        height: 281px;
        overflow: hidden;
    }
    .cssload-wrap {
        position: absolute;
        width: 188px;
        height: 188px;
        left: 50%;
        top: 50%;
        margin-top: -94px;
        margin-left: -94px;
        transform: scale(2);
        -o-transform: scale(2);
        -ms-transform: scale(2);
        -webkit-transform: scale(2);
        -moz-transform: scale(2);
    }
    .cssload-wrap .cssload-overlay {
        position: absolute;
        width: 100%;
        height: 100%;
        z-index: 100;
        box-shadow: 0 0 47px 71px rgb(255,255,255) inset;
        -o-box-shadow: 0 0 47px 71px rgb(255,255,255) inset;
        -ms-box-shadow: 0 0 47px 71px rgb(255,255,255) inset;
        -webkit-box-shadow: 0 0 47px 71px rgb(255,255,255) inset;
        -moz-box-shadow: 0 0 47px 71px rgb(255,255,255) inset;
    }
    .cssload-wrap .cssload-cogWheel {
        position: absolute;
        top: 50%;
        margin-top: -47px;
        width: 94px;
        height: 94px;
    }
    .cssload-wrap .cssload-cogWheel.cssload-one {
        left: -11.25px;
        animation: cssload-rotLeft 1.15s infinite linear;
        -o-animation: cssload-rotLeft 1.15s infinite linear;
        -ms-animation: cssload-rotLeft 1.15s infinite linear;
        -webkit-animation: cssload-rotLeft 1.15s infinite linear;
        -moz-animation: cssload-rotLeft 1.15s infinite linear;
    }
    .cssload-wrap .cssload-cogWheel.cssload-one .cssload-one:before {
        border: none;
    }
    .cssload-wrap .cssload-cogWheel.cssload-two {
        right: -11.25px;
        margin-top: -45px;
        transform: rotate(196deg);
        -o-transform: rotate(196deg);
        -ms-transform: rotate(196deg);
        -webkit-transform: rotate(196deg);
        -moz-transform: rotate(196deg);
        animation: cssload-rotRight 1.15s infinite linear;
        -o-animation: cssload-rotRight 1.15s infinite linear;
        -ms-animation: cssload-rotRight 1.15s infinite linear;
        -webkit-animation: cssload-rotRight 1.15s infinite linear;
        -moz-animation: cssload-rotRight 1.15s infinite linear;
    }
    .cssload-wrap .cssload-cogWheel.cssload-two .cssload-one:before {
        border: none;
    }
    .cssload-wrap .cssload-cogWheel .cssload-cog {
        position: absolute;
        width: 100%;
        left: 0;
        top: 50%;
    }
    .cssload-wrap .cssload-cogWheel .cssload-cog:after {
        display: block;
        position: absolute;
        content: ' ';
        border: 4px solid rgb(0,0,0);
        border-left: none;
        height: 17px;
        width: 9px;
        right: -17px;
        top: -8px;
    }
    .cssload-wrap .cssload-cogWheel .cssload-cog:before {
        display: block;
        position: absolute;
        content: ' ';
        border-left: 4px solid rgb(0,0,0);
        height: 19px;
        width: 8px;
        right: -11px;
        top: -24px;
        transform: rotate(-20deg);
        -o-transform: rotate(-20deg);
        -ms-transform: rotate(-20deg);
        -webkit-transform: rotate(-20deg);
        -moz-transform: rotate(-20deg);
    }
    .cssload-wrap .cssload-cogWheel .cssload-cog.cssload-one {
        transform: rotate(-80deg);
        -o-transform: rotate(-80deg);
        -ms-transform: rotate(-80deg);
        -webkit-transform: rotate(-80deg);
        -moz-transform: rotate(-80deg);
    }
    .cssload-wrap .cssload-cogWheel .cssload-cog.cssload-two {
        transform: rotate(-40deg);
        -o-transform: rotate(-40deg);
        -ms-transform: rotate(-40deg);
        -webkit-transform: rotate(-40deg);
        -moz-transform: rotate(-40deg);
    }
    .cssload-wrap .cssload-cogWheel .cssload-cog.cssload-three {
        transform: rotate(0deg);
        -o-transform: rotate(0deg);
        -ms-transform: rotate(0deg);
        -webkit-transform: rotate(0deg);
        -moz-transform: rotate(0deg);
    }
    .cssload-wrap .cssload-cogWheel .cssload-cog.cssload-four {
        transform: rotate(40deg);
        -o-transform: rotate(40deg);
        -ms-transform: rotate(40deg);
        -webkit-transform: rotate(40deg);
        -moz-transform: rotate(40deg);
    }
    .cssload-wrap .cssload-cogWheel .cssload-cog.cssload-five {
        transform: rotate(80deg);
        -o-transform: rotate(80deg);
        -ms-transform: rotate(80deg);
        -webkit-transform: rotate(80deg);
        -moz-transform: rotate(80deg);
    }





    @keyframes cssload-rotLeft {
        from {
            transform: rotate(-30deg);
        }
        to {
            transform: rotate(10deg);
        }
    }

    @-o-keyframes cssload-rotLeft {
        from {
            transform: rotate(-30deg);
        }
        to {
            transform: rotate(10deg);
        }
    }

    @-ms-keyframes cssload-rotLeft {
        from {
            transform: rotate(-30deg);
        }
        to {
            transform: rotate(10deg);
        }
    }

    @-webkit-keyframes cssload-rotLeft {
        from {
            transform: rotate(-30deg);
        }
        to {
            transform: rotate(10deg);
        }
    }

    @-moz-keyframes cssload-rotLeft {
        from {
            transform: rotate(-30deg);
        }
        to {
            transform: rotate(10deg);
        }
    }

    @keyframes cssload-rotRight {
        from {
            transform: rotate(-174deg);
        }
        to {
            transform: rotate(-214deg);
        }
    }

    @-o-keyframes cssload-rotRight {
        from {
            transform: rotate(-174deg);
        }
        to {
            transform: rotate(-214deg);
        }
    }

    @-ms-keyframes cssload-rotRight {
        from {
            transform: rotate(-174deg);
        }
        to {
            transform: rotate(-214deg);
        }
    }

    @-webkit-keyframes cssload-rotRight {
        from {
            transform: rotate(-174deg);
        }
        to {
            transform: rotate(-214deg);
        }
    }

    @-moz-keyframes cssload-rotRight {
        from {
            transform: rotate(-174deg);
        }
        to {
            transform: rotate(-214deg);
        }
    }


</style>
