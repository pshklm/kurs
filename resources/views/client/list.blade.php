@extends('layouts.layout')
@section('page_title')
    <b>Список</b>
@endsection

<?php
$model_clients = new \App\Models\Clients();
//$regulars = $model_clients->getRegularTypesForClients();
//$abonements = $model_clients->getAbonements();
?>

@section('content')
    <form method="get" action="/client">
        <select class="select-css", name="type">
            <option value="0">All types of abonement</option>

            @foreach($abonement_types as $type_id => $type_title)
                <option value="{{$type_id}}"
                    {{($type_id == $type_selected) ? 'selected' : ''}}>
                    {{$type_title}}
                </option>
            @endforeach

            {{--            @foreach($abonements as $abonement_id)--}}
            {{--                <option value="{{$abonement_id->abonement}}"--}}
            {{--                {{($abonement_id == $abonement_selected) ? 'selected' : ''}}>--}}
            {{--                {{$abonement_id->$abonement}}--}}
            {{--            @endforeach--}}
        </select>
        <select class="select-css", name="regular">
            <option value="0">All clients</option>

            @foreach($regular_types as $regular_id =>$regular_title)
                <option value="{{$regular_id}}"
                    {{($regular_id == $regular_selected) ? 'selected' : ''}}>
                    {{$regular_title}}
                </option>
            @endforeach

            {{--            @foreach($regulars as $regular_id)--}}
            {{--                <option value="{{$regular_id->regular}}"--}}
            {{--                    {{( $regular_id == $regular_selected) ? 'selected' : ''}}>--}}
            {{--                    {{$regular_id->regular}}--}}
            {{--                </option>--}}
            {{--            @endforeach--}}
        </select>
        <input type="submit" value="FIND"/>
    </form>
<br>
    <br>
    <br>

    <table>
        <th> Full Name</th>
        <th>Personal ID</th>
        <th>Tel(+380)</th>
        <th>Date of registration</th>
        @foreach($clients as $client)
            <tr>
                <td>
                    <a href="/client/{{$client->client_id}}">
                        {{$client->name}}
                    </a>
                </td>
                <td>{{$client->client_id}}</td>
                <td>{{$client->tel}}</td>
                <td>{{$client->date}}</td>
                {{--                <td><a href="/edit/{{$client->client_id}}" > Edit</a> </td>--}}
            </tr>

        @endforeach
    </table>


    <form method='GET' action="/del">
        <input type='text' name='client_id' placeholder='Укажите ИД'/>
        <input type='submit' value='Удалить'>
    </form>

    <p>Добавление клиента</p>

    <form method='GET' action="/adding">
        <input type='text' name='name' placeholder='Укажите Имя'/>
        <input type='number' name='subs_id' placeholder='Укажите subscription_id'/>
        <input type='number' name='tel' placeholder='Укажите tel'/>
        <input type='submit' value='Добавить'>
    </form>

{{--    <p>Edit клиента</p>--}}
{{--    <form method='GET'>--}}
{{--        <input type='text' name='client_id' placeholder='Укажите client_id''/></p>--}}
{{--        <input type='text' name='name' placeholder='Укажите новое имя' /></p>--}}
{{--        <input type='number' name='type_sbs' placeholder='Укажите ти абонемента'/></p>--}}
{{--        <input type='date' name='datee' placeholder='data start' /></p>--}}
{{--        <input type='date' name='dates' placeholder='data end' /></p>--}}

{{--        <input type='submit' value='Сохранить'>--}}
{{--    </form>--}}
@endsection
